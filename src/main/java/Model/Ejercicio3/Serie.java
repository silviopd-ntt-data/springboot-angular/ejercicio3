package Model.Ejercicio3;

public class Serie implements Entregable {

    public static final int NUMERO_TEMPORADAS_DEFAULT = 3;

    private String titulo = "", genero = "", creador = "";
    private int nroTemporadas = NUMERO_TEMPORADAS_DEFAULT;
    private boolean entregado = false;


    public Serie() {
    }

    public Serie(String titulo, String creador) {
        this.titulo = titulo;
        this.creador = creador;
    }

    public Serie(String titulo, String genero, String creador, int nroTemporadas) {
        this.titulo = titulo;
        this.genero = genero;
        this.creador = creador;
        this.nroTemporadas = nroTemporadas;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getGenero() {
        return genero;
    }

    public String getCreador() {
        return creador;
    }

    public int getNroTemporadas() {
        return nroTemporadas;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public void setNroTemporadas(int nroTemporadas) {
        this.nroTemporadas = nroTemporadas;
    }



    @Override
    public String toString() {
        return "Serie{" +
                "titulo='" + titulo + '\'' +
                ", genero='" + genero + '\'' +
                ", creador='" + creador + '\'' +
                ", nroTemporadas=" + nroTemporadas +
                ", entregado=" + entregado +
                '}';
    }

    @Override
    public boolean entregar() {
        return this.entregado = true;
    }

    @Override
    public boolean devolver() {
        return this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return this.entregado;
    }

    @Override
    public int compareTo(Object a) {
        return 0;
    }
}
